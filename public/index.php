<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

$cookie_params = session_get_cookie_params();
$cookie_params['httponly'] = TRUE;
$cookie_params['samesite'] = 'None';
$cookie_params['secure'] = TRUE;
$result = session_set_cookie_params($cookie_params);
if ($result !== TRUE) {
  throw new Exception("Error setting cookie params to: " . print_r($cookie_params, TRUE) . ": " . print_r(error_get_last(), TRUE));
}
session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();

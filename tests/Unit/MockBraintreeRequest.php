<?php

namespace Tests\Unit;

class MockBraintreeRequest {
  public function __construct($notification_type) {
    $this->notification_type = $notification_type;
    $this->notification_vars = \Braintree\WebhookTesting::sampleNotification($notification_type, 'test');
  }

  public function getParsedBody() {
    return $this->notification_vars;
  }
}

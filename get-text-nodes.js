let fs = require('fs');
var	opts = require('tav').set();
let jsdom = require("jsdom");
let { JSDOM } = jsdom;

if (typeof opts.args[0] == 'undefined') {
  throw 'You must enter a filename';
}

html = fs.readFileSync(opts.args[0], 'utf8');
const dom = new JSDOM(html);
let allTextNodes = textNodesUnder(dom, dom.window.document.body);
for (const text of allTextNodes) {
  const content = text.textContent.trim();
  if (content != "") {
    console.log(content);
  }
}

function textNodesUnder(dom, el) {
  var n, a=[], walk=dom.window.document.createTreeWalker(el,dom.window.NodeFilter.SHOW_TEXT,null,false);
  while(n=walk.nextNode()) a.push(n);
  return a;
}

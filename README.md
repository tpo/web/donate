# Tor Donation

## Environment Setup

``` bash
yarn install
apt install redis
```

Create a file at private/settings.local.php.
It should have this in it:

``` php
<?php

$stripeSettings['secretKey'] = 'Input-SecretKey-here(Sylvanus)';
$stripeSettings['publishableKey'] = 'Input-PublishableKey-here(Sylvanus)';
```

For local developments only, add the following line in private/settings.local.php.
```

$torSiteBaseUrl = 'http://localhost:5000';
```

You'll need to setup your Apache config to point to the public subdirectory in the repository.

Now to do development, just run:

``` bash
yarn watch
```

## Commiting changes

There's a complicated procedure for commiting changes because there's some localization scripts that need to run and also we want to include the latest public/dist compiled for production.

When you are ready to commit some changes, you need to do this:
``` bash
yarn ready-commit
```

## Testing time dependendent pages

For time-triggered switch testing (e.g: promo that starts and ends at a certain time, etc) in an environment, simulate "now" override by creating a file called `now_override.php` inside `/private` and paste the following inside that file and edit accordingly for your test. Reminder that UTC = EST + 5 hours, so the example below is for 12AM on Dec 30, 2018. This file should never exist in the production site.

``` php
<?php

$now_override = \DateTime::createFromFormat('Y-m-j-H:i:s', '2018-12-30-05:00:00', new \DateTimeZone('UTC'));
```

## Donation System Overview

Public facing Tor donation site built on top of slim site (donate.torproject.org)
Private civicrm site (civicrm.torproject.org)
Redis/Resque service running on same server as civicrm. We had the Tor sysadmins set up a permanent tunnel that forwards connections to redis on the donate server localhost to redis on the civicrm server. Resque is messaging system on top of redis, it's a php library. Resque is based off of another thing built by github in ruby that is also called resque.

The donation script sends a class name and args to the redis. The processor on the CRM side gets the classname/args from reids, instantiates the class using the args and saves them to Civi.

## Donation Flow

Tor donation site sends payment request to stripe
Stripe responds with payment info
Takes the payment info and writes it to resque

Processor on civicrm site uses redis to process each payment message
You run this processor with:
````
cd sites/all/modules/custom/tor_donation/
APP_ENV=dev ./bin/processor
````
There's a cron entry that starts the processor_loop when the server is restarted. processor_loop will restart the processor script if it dies.
Peter wanted to use a service but Tor people didn't want to do it that way. So the processor loop is a simple bash script.

## Recurring donations

Webhooks in slim one for paypal one for stripe, when they are called they take the donation information and put it in the redis queue and the processor processes the recurring donation and sticks it into civi.

Paypal sends two messages for recurring -- once for when it's set up and once for when it's been charged. They can come out of order. So if the charge comes before it's set up, we stick it back on the end of the redis queue so it will get handled in the correct order. The problem is that there's a bunch in the queue that are just wrong, so they are perpetually stuck in the queue. There are some recurring donations in paypal that don't exist in civi possibly.

For more info on how to test recurring donations locally and on staging.tordonate.org, go to the civicrm repo and see the readme at sites/all/modules/custom/tor_donation/README.md.

## Donation counts

See sites/all/modules/custom/tor_donation/tor_donation.module

Every time a civi contribution is created or modified it runs tor_donation_update_counter, runs a query which totals up all donations since the time set in the drupal config setting. updates the two counters in redis:
resque:dev_donationCounter.totalAmount
resque:dev_donationCounter.totalContributors
(prod uses "prod" instead of "dev")

## To log into Redis

run redis-cli
```
set resque:dev_donationCounter.totalAmount 1000
set resque:dev_donationCounter.totalContributors 10
```
show things in list from 0 - 100
```
lrange resque:queue:dev_web_donations 0 100
```
The entries in resque:queue:dev_web_donations are serialized php. The processor takes these objects and uses methods in the classes to store them in civicrm.

# Tor Localization

There are several commands for rebuilding localization files. You must have gettext as well as the php gettext extension installed for this to work.

The list of available localizations lives in translation/locale.json. All of these locales must be installed on your system for the php gettext extension to work properly.

To read more basics on setting gettext + php-gettext up with twig, see:

* http://twig-extensions.readthedocs.io/en/latest/i18n.html
* https://www.sitepoint.com/easy-multi-language-twig-apps-with-gettext/

## Procedure for getting changes translated

Before you commit any change, run this command:
`
gulp locale-xgettext
`

If there are any changes you made that need translation, this command will modify the translation/out/messages.pot file. You should check in those changes with your commit. When the commit is merged to master, you should tell Peter to push this also to Tor's repository.

## Procedure for updating to latest translations from Transifex

````
cd translation/in
git checkout donatepages-messagespot_completed
git pull
cd ../..
gulp locale-msgfmt
git add translation
git commit
````

We currently get a lot of warnings about "Message conversion to user's charset will not work." and "warning: header field missing in header". I think we can ignore those.

## Installing a new locale on Debian/Linux

I think you only need to do this if you want to get the web page to actually
do the translation. You don't need these to create the messages.pot file or
to import translations from Transifex.

````
sudo locale-gen es es_ES es_ES.UTF-8
sudo update-locale
sudo dpkg-reconfigure locales
sudo systemctl restart php7.2-fpm.service
````

## Adding a new locale

Add the locale to translation/locale.json and src/settings.php.

### Search for non-translated strings in html files

The helper script get-text-nodes.js will parse and html file and output all the text nodes on the screen so you can see any potentially untranslated strings. (If they are translated the strings should start with {% trans %} and end with {% endtrans $}.

````
node get-text-nodes.js PATH_TO_TWIG_FILE
````

## Manual Workflow

You don't need this if you're using the gulp commands. This is documentation for how the whole thing works.

## Adding a new locale manually

Run the following commands, replacing ${YOUR\_NEW\_LOCAL} with the name of the new locale:

````
mkdir -p locale/${YOUR_NEW_LOCALE}/LC_MESSAGES
msginit --locale=${YOUR_NEW_LOCALE}--output-file=locale/${YOUR_NEW_LOCALE}/LC_MESSAGES/messages.po --input=locale/messages.pot
````

### Regenerating the list of translatable strings manaully

The i18n.php file loads twig and builds a cache of all the twig files and puts them into the tmp/cache\_locale directory. Additionally, to run the xggettext command listed below, you'd need to have globstar enabled in bash or use a different command (e.g. find) to list out all of the php files under tmp/cache\_locale.

````
php i18n.php
xgettext -o locale/messages.pot --add-comments=notes --from-code=UTF-8 -n --omit-header tmp/cache_locale/**/*.php
````

Then for each locale, you'd have to merge the new messages from the locale/messages.pot file into the
existing .po files.  Foreach locale, run the following, swapping ${LOCAL\_NAME} for the name of the locale.

````
msgmerge -U locale/${LOCALE_NAME}/LC_MESSAGES/messages.po locale/messages.pot
````

### Rebuilding the .mo files manually

Run the following msgffmt command for each locale, replacing ${LOCALE\_NAME} with the name of the locale.

````
msgfmt -c -o locale/${LOCALE_NAME}/LC_MESSAGES/messages.mo locale/${LOCALE_NAME}/LC_MESSAGES/messages.po
````

## PayPal and first party isolation

There are a some work-arounds for if you turn on "first party isolation" in the
web browser. That limits some types of cookie operations. I think the PayPal
javascript library uses a cookie to communicate between the popup window and
the PayPal iframe in the original page, but "first party isolation" prevents
this communication.

When PayPal's javascript library is working normally, after the user authorizes
the payment in the PayPal popup window, the javascript library calls a callback
in our code with a token that represents the payment. We then send that to PHP
via an AJAX POST to /process-paypal which then PayPal via the PHP API to
complete the Payment.

When things are not working normally, instead of calling the callback, PayPal
redirects the popup window to GET /process-paypal with the token as a query
parameter. When that happens, we just do the same thing we do when we get the
AJAX POST to /process-paypal and complete the payment using the token. The
problem is that the request is coming from the popup window and not the
original donate window, so we need to get that popup window to close and go
back to the donate window and then go to the thank-you page. So the rest of the
javascript involving the timeout and local storage is to figure out when we
should close the popup and redirect to the thank-you page.

<?php

namespace Tor;

class DonateException extends \Exception {
  public $error_field_id;
  function __construct($message, $error_field_id = NULL) {
    $this->error_field_id = $error_field_id;
    parent::__construct($message);
  }

  function getErrorFieldID() {
    return $this->error_field_id;
  }
}

<?php
// DIC configuration

$container = $app->getContainer();

$environmentInfo = new \Tor\EnvironmentInfo($container);
$container['environment_info'] = $environmentInfo;

// view renderer
$container['renderer'] = function ($container) {
  $settings = $container->get('settings')['renderer'];
  $environment_info = $container->environment_info;
  $cache = FALSE;
  if (!$environment_info->is_dev()) {
    $cache = $settings['cache_path'];
  }
  $view = new \Slim\Views\Twig($settings['template_path'], [
    'cache' => $cache,
  ]);
  $request = $container['request'];
  $view->addExtension(new \Slim\Views\TwigExtension(
    $container['router'],
    $request->getUri()
  ));

  $baseUrl = $request->getUri()->getBaseUrl();
  $view->offsetSet('baseUrl', $baseUrl);

  // load i18n twig extension
  $view->addExtension(new Twig_Extensions_Extension_I18n());

  return $view;
};


// monolog
$container['logger'] = function ($container) {
  $settings = $container->get('settings')['logger'];
  $environmentInfo = $container->get('environment_info');
  $logger = new Monolog\Logger($settings['name']);
  $logger->pushProcessor(new Monolog\Processor\UidProcessor());
  $logger->pushProcessor(new \Monolog\Processor\ProcessIdProcessor());
  $logLevel =  \Monolog\Logger::INFO;
  if (!$environmentInfo->is_prod()) {
    $logLevel =  \Monolog\Logger::DEBUG;
  }
  $stream_handler = new Monolog\Handler\StreamHandler($settings['path'], $logLevel);
  $formatter = new \Tor\LineFormatter();
  $formatter->includeStacktraces(TRUE);
  $formatter->allowInlineLineBreaks(TRUE);
  $stream_handler->setFormatter($formatter);
  $logger->pushHandler($stream_handler);
  if ($environmentInfo->is_prod()) {
    $error_recipients = array(
      'Coop Symbiotic <mathieu+torlog@symbiotic.coop>',
      'Tor Donations <donation-drivers@torproject.org>',
    );
    $email_handler = new Monolog\Handler\NativeMailerHandler($error_recipients, "Error on donation website", 'Donation Website <donation-web@torproject.org>');
    $email_handler->setFormatter($formatter);
    $logger->pushHandler($email_handler, Monolog\Logger::ERROR);
  }
  return $logger;
};

$container['logger_ratelimit'] = function ($container) {
  $settings = $container->get('settings')['logger_ratelimit'];
  $environmentInfo = $container->get('environment_info');
  $logger = new Monolog\Logger($settings['name']);
  $logger->pushProcessor(new Monolog\Processor\UidProcessor());
  $logger->pushProcessor(new \Monolog\Processor\ProcessIdProcessor());
  $logLevel =  \Monolog\Logger::INFO;
  $stream_handler = new Monolog\Handler\StreamHandler($settings['path'], $logLevel);
  $formatter = new \Tor\LineFormatter();
  $stream_handler->setFormatter($formatter);
  $logger->pushHandler($stream_handler);
  return $logger;
};

$container['errorHandler'] = function($container) {
  $error_handler = new \Tor\ErrorHandler($container);
  return $error_handler;
};

$container['phpErrorHandler'] = function($container) {
  $error_handler = new \Tor\PhpErrorHandler($container);
  return $error_handler;
};

$container['ipRateLimiter'] = function($container) {
  return new \Tor\IpRateLimiter($container);
};
$container['emailRateLimiter'] = function($container) {
  return new \Tor\EmailRateLimiter($container);
};

$settings = $container->get('settings');
$settings['redis'] = [
  'host' => 'localhost',
  'port' => 6379,
];
if ($environmentInfo->name() == 'prod' || $environmentInfo->name() == 'staging') {
  $settings['redis'] = [
    'host' => 'crm-int-01-priv.torproject.org',
    'port' => 6379,
  ];
  Resque::setBackend("{$settings['redis']['host']}:{$settings['redis']['port']}");
}

$container['hmac'] = function($container) {
  $secret_key = $container->get('settings')['secret_key'];
  return function($hash_function, $data) {
    return hash_hmac($hash_function, $data, $secret_key);
  };
};

$container['ratelimit'] = function($container) {
  return $container->get('settings')['ratelimit'];
};

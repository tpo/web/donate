<?php

namespace Tor;

class CrmController extends BaseController {
  public function sendMessage($className, $messageInfo) {
    $queue_name = \Tor\Resque::queueName($this->environment_info);
    try {
      \Resque::enqueue($queue_name, $className, $messageInfo);
    } catch (\Exception $e) {
      $message = sprintf('Error sending message to Redis %s %s: "%s" at %s line %s', $className, get_class($e), $e->getMessage(), $e->getFile(), $e->getLine());
      $message = "$message\nMessage Info: " . print_r($messageInfo, TRUE);
      $this->logger->error($message, array('exception' => $e, 'trace' => $e->getTrace()));
    }
  }
}

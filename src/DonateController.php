<?php

namespace Tor;

use PayPal\Core\PPConfigManager;
use Slim\Http\Uri;
use Tor\ArrayExt;
use Tor\CampaignController;

class DonateController extends BaseController {
  public $vars = array();

  public function index($request, $response, $args) {
    $campaignController = new CampaignController($this->container);
    $now = $this->environment_info->now();
    $templateToRender = 'index.twig';

    $config = PPConfigManager::getConfigWithDefaults();
    $stripeConfig = StripeConfig::setup();
    $this->vars = array(
      'environmentName' => $this->environment_info->name(),
      'isMatchingDonation' => CampaignController::is_matching_donation($now),
      'paypalMerchantId' => $config['acct1.MerchantID'],
      'stripePublishableKey' => $stripeConfig->publishableKey,
      'langcode' => $request->getAttribute('language'),
      'headerHasBgImg' => TRUE,
      'bodyClasses' => ['front', 'title-header-image'],
    );
    return $this->renderer->render($response, $templateToRender, $this->vars);
  }

  public function monthly($request, $response, $args) {
    $campaignController = new CampaignController($this->container);
    $now = $this->environment_info->now();
    $templateToRender = 'monthly-giving.twig';
    $paypalEnvironment = $this->environment_info->paypalEnvironment();
    $paypalClientId = $this->container->get('settings')['paypalClientId'][$paypalEnvironment];

    $stripeConfig = StripeConfig::setup();
    $this->vars = array(
      'environmentName' => $this->environment_info->name(),
      'isMatchingDonation' => CampaignController::is_matching_donation($now),
      'paypalClientId' => $paypalClientId,
      'stripePublishableKey' => $stripeConfig->publishableKey,
      'langcode' => $request->getAttribute('language'),
      'headerHasBgImg' => TRUE,
      'bodyClasses' => ['monthly-giving', 'title-header-image'],
    );
    return $this->renderer->render($response, $templateToRender, $this->vars);
  }

  public function donor_faq($request, $response, $args) {
    $this->vars = array(
      'langcode' => $request->getAttribute('language')
    );
    return $this->renderer->render($response, 'donor-faq.twig', $this->vars);
  }

  public function state_disclosures($request, $response, $args) {
    $this->vars = array(
      'langcode' => $request->getAttribute('language')
    );
    return $this->renderer->render($response, 'state-disclosures.twig', $this->vars);
  }

  public function privacy_policy($request, $response, $args) {
    $this->vars = array(
      'langcode' => $request->getAttribute('language')
    );
    return $this->renderer->render($response, 'privacy-policy.twig', $this->vars);
  }

  public function thank_you($request, $response, $args) {
    $uri = $request->getUri();
    $baseUrl = $uri->getBaseUrl();
    $now = $this->environment_info->now();
    $this->vars = array(
      'baseUrl' => $baseUrl,
      'encodedBaseUrl' => urlencode($baseUrl),
      'langcode' => $request->getAttribute('language'),
      'isMatchingDonation' => CampaignController::is_matching_donation($now),
    );
    return $this->renderer->render($response, 'thank-you.twig', $this->vars);
  }

  function championsOfPrivacy($request, $response, $args) {
    $campaignController = new CampaignController($this->container);
    $now = $this->environment_info->now();
    $templateToRender = 'champions-of-privacy.twig';
    $paypalEnvironment = $this->environment_info->paypalEnvironment();
    $paypalClientId = $this->container->get('settings')['paypalClientId'][$paypalEnvironment];

    $stripeConfig = StripeConfig::setup();
    $this->vars = array(
      'environmentName' => $this->environment_info->name(),
      'isMatchingDonation' => CampaignController::is_matching_donation($now),
      'paypalClientId' => $paypalClientId,
      'stripePublishableKey' => $stripeConfig->publishableKey,
      'langcode' => $request->getAttribute('language'),
      'headerHasBgImg' => TRUE,
      'bodyClasses' => ['champions-of-privacy', 'title-header-image'],
    );
    return $this->renderer->render($response, $templateToRender, $this->vars);
  }
}

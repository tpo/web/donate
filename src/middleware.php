<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
$app->add(new \Tor\I18nMiddleware($app));
$app->add(new \Tor\AccessControlMiddleware($app));
$app->add(new  RKA\Middleware\IpAddress());

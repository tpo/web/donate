<?php

namespace Tor;

class Uri extends \Slim\Http\Uri  {
  public $queryParams = [];

  public function addQueryParam($name, $value) {
    $this->queryParams[$name] = $value;
  }

  public static function buildQueryString($queryParams) {
    return http_build_query($queryParams);
  }

  public function __toString() {
    $this->query = static::buildQueryString($this->queryParams);
    return parent::__toString();
  }

  public function toString() {
    return (string) $this;
  }
}

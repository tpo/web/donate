<?php

namespace Tor;

class Captcha {
  function generate($request, $response, $args) {
    $letters = substr(str_shuffle("ACDJKMNOPSTVWXYZ"),0,4);
    $image = imagecreate(40,13);
    imagecolorallocate($image, 255, 255, 255);
    imagecolorallocate($image, 0, 0, 0);
    imagecolorallocate($image, rand(50,175), rand(125,225), rand(125,225));
    imagecolorallocate($image, rand(125,225), rand(50,175), rand(125,225));
    imageline($image, 0, rand(9,14), 40, rand(2,7), 2);
    imagestring($image, 5, 3, 0, " " . $letters[0] . " " . $letters[1], 1);
    imagestring($image, 5, 3, 0, $letters[2] . " " . $letters[3], 1);
    $_SESSION["captcha"] = hash('sha256', $letters[2] . $letters[0] . $letters[3] . $letters[1]);

    $stream = fopen('php://memory', 'r+');
    imagepng($image, $stream);
    rewind($stream);
    $content = stream_get_contents($stream);
    $response->write($content);
    return $response->withHeader('Content-Type', 'image/png');
  }

  function is_valid($fields, $session) {
    $valid = TRUE;
    $captcha = trim(ArrayExt::fetch($fields, "captcha"));
    if (($captcha == '') || (hash("sha256", strtoupper($captcha)) != $session["captcha"])) {
      $valid = FALSE;
    }

    # XXX: we should abstract this into a "validate_request" function
    # but for now, let's be fast and messy

    require(__DIR__ . '/../private/settings.local.php');
    if (in_array($_SERVER['REMOTE_ADDR'], $ipDenyList, true)) {
        $valid = false;
    }

    unset($session["captcha"]);

    return $valid;
  }
}

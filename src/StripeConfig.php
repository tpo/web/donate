<?php

namespace Tor;

class StripeConfig {
  private static $instance;
  public $secretKey;
  public $publishableKey;

  protected function __construct() {
    $this->configure();
  }

  private function __clone() {
  }

  private function __wakeup() {
  }

  public static function setup() {
    if (static::$instance === NULL) {
      static::$instance = new static();
    }
    return static::$instance;
  }

  public function configure() {
    require(__DIR__ . "/../private/settings.local.php");
    $this->secretKey = $stripeSettings['secretKey'];
    $this->publishableKey = $stripeSettings['publishableKey'];
    \Stripe\Stripe::setApiKey($this->secretKey);
  }
}

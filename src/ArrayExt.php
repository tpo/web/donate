<?php

namespace Tor;

class ArrayExt {
  public static function fetch($array, $key, $default = NULL) {
    if (array_key_exists($key, $array)) {
      return $array[$key];
    } else {
      return $default;
    }
  }

  public static function fetchRequired($array, $key) {
    if (array_key_exists($key, $array)) {
      return $array[$key];
    } else {
      throw new KeyNotFound("Expected array to have '$key'");
    }
  }
}

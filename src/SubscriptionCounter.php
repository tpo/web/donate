<?php

namespace Tor;

class SubscriptionCounter {
  public $redis;
  public $environment_info;

  public function __construct($container) {
    $this->environment_info = $container->get('environment_info');
    $this->logger = $container->get('logger');
    $this->redis = \Resque::redis();
  }

  public function countSubscriptionRequest() {
    $this->redis->incr($this->environment_info->name() . "_subscription_request_count");
  }

  public function countSubscriptionConfirmed() {
    $this->redis->incr($this->environment_info->name() . "_subscription_confirmed_count");
  }
}

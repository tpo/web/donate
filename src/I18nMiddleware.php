<?php

namespace Tor;

use \Symfony\Component\HttpFoundation\AcceptHeader;
use \Symfony\Component\HttpFoundation\AcceptHeaderItem;

class I18nMiddleware {

  const LANGUAGE_REGEX = '/^([a-z]{2}(?:_[A-Z]{2})?)$/';
  public $container;
  public $request;
  public $acceptLanguageHeader;
  public $availableLanguages;
  public $defaultLanguage;
  public $preferredLanguages;
  public $language;

  public function __invoke($request, $response, $next) {
    $uri = $request->getUri();
    $this->request = $request;
    $this->setLanguageFromPreferred();
    $path = $uri->getPath();
    $args = explode('/', $path);
    $language_arg = $this->matchLanguageFromArgument($args[1]);
    if ($language_arg) {
      $this->setLanguage($language_arg);
      $new_path = $this->stripLanguageFromPath($path);
      $uri = $uri->withPath($new_path);
      $this->request = $this->request->withUri($uri);
    }
    $request = $this->request;
    $response = $next($request, $response);
    return $response;
  }

  function __construct($app) {
    $this->container = $app->getContainer();
    $this->defaultLanguage = $this->container->get('settings')['language_default'];
    $this->availableLanguages = $this->container->get('settings')['languages'];
  }

  function matchLanguageFromArgument($arg) {
    $language = NULL;
    $matches = array();
    preg_match(self::LANGUAGE_REGEX, $arg, $matches);
    if (!empty($matches)) {
      $language = $matches[1];
    }
    return $language;
  }

  function stripLanguageFromPath($path) {
    $args = explode('/', $path);
    $args = array_slice($args, 2); 
    $new_path = '/' . implode('/', $args);
    return $new_path; 
  }
  
  /**
   * Gets a list of languages acceptable by the client browser.
   * Taken from Symfony\Component\HttpFoundation\Request; 
   *
   * @return array Languages ordered in the user browser preferences
   */
  function setPreferredLanguagesFromAcceptLanguagesHeader() {
    if (null !== $this->preferredLanguages) {
        return;
    }
    $languages = AcceptHeader::fromString($this->request->getHeaderLine('Accept-Language'))->all();
    $this->preferredLanguages = array();
    foreach ($languages as $lang => $acceptHeaderItem) {
      if (false !== strpos($lang, '-')) {
        $codes = explode('-', $lang);
        if ('i' === $codes[0]) {
          // Language not listed in ISO 639 that are not variants
          // of any listed language, which can be registered with the
          // i-prefix, such as i-cherokee
          if (count($codes) > 1) {
              $lang = $codes[1];
          }
        } 
        else {
          for ($i = 0, $max = count($codes); $i < $max; ++$i) {
            if ($i === 0) {
                $lang = strtolower($codes[0]);
            } 
            else {
                $lang .= '_'.strtoupper($codes[$i]);
            }
          }
        }
      }

      $this->preferredLanguages[] = $lang;
    }
  }

  function setLanguageFromPreferred() {
    $this->setPreferredLanguagesFromAcceptLanguagesHeader();
    foreach ($this->preferredLanguages as $language) {
      if (isset($this->availableLanguages[$language])) {
        $this->setLanguage($language);
        break;
      }
    }
    if ($this->language == NULL) {
      $this->setLanguage($this->defaultLanguage);
    }
  }

  public function setLanguage($langcode) {
    if (!isset($this->availableLanguages[$langcode])) {
      throw new \Slim\Exception\NotFoundException($this->container->request, $this->container->response);
    }
    
    $language = $this->availableLanguages[$langcode];
    $this->language = $language;
    $this->request = $this->request->withAttribute('language', $language);

    putenv("LANGUAGE=" . $language); 
    setlocale(LC_ALL, $language);

    $domain = "messages";
    bindtextdomain($domain, __DIR__. "/../translation/built/locale");
    bind_textdomain_codeset($domain, 'UTF-8');

    textdomain($domain);
  }

}

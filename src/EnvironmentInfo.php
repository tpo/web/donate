<?php

namespace Tor;

class EnvironmentInfo {
  public $container;
  private $app_env;

  function __construct($container) {
    $this->container = $container;
    $this->app_env = getenv('APP_ENV');
    if (!$this->app_env) {
        $this->app_env = 'prod';
    }
  }

  function name() {
    return $this->app_env;
  }

  function is_dev() {
    return $this->name() == 'dev';
  }

  function is_prod() {
    return $this->name() == 'prod';
  }

  function now() {
    if (file_exists(__DIR__ . "/../private/now_override.php") && !$this->is_prod()) {
      require(__DIR__ . "/../private/now_override.php");
      $now = $now_override;
      if (!is_a($now, 'DateTime')) {
        throw new \Exception('$now_override in /private/now_override.php is not a properly created DateTime Object.');
      }
    }
    else {
      $now = new \DateTime('now', new \DateTimeZone('UTC'));
    }
    return $now;
  }

  function paypalEnvironment() {
    if ($this->is_prod()) {
      return 'production';
    }
    else {
      return 'sandbox';
    }
  }
}

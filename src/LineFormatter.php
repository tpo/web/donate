<?php

namespace Tor;

class LineFormatter extends \Monolog\Formatter\LineFormatter {
  public function __construct() {
    $format = "[%datetime%] %channel%[%extra.process_id%].%level_name%: (%extra.uid%) %message%\n%backtrace%";
    parent::__construct($format, NULL, TRUE, TRUE);
    $this->includeStacktraces(TRUE);
  }

  public function format(array $record) {
    $context = ArrayExt::fetch($record, 'context');
    $record['backtrace'] = '';
    if ($context !== NULL) {
      $message = ArrayExt::fetch($record, 'message');
      if ($message !== NULL) {
        foreach ($record['context'] as $var => $val) {
          $message = str_replace('{' . $var . '}', $this->stringify($val), $message);
        }
        $record['message'] = $message;
      }
      $trace = ArrayExt::fetch($context, 'trace');
      if ($trace !== NULL) {
        $backtrace = $this->traceToString($trace);
        $record['backtrace'] = $backtrace;
      }
    }
    return parent::format($record);
  }

  function traceToString($trace) {
    $result = "";
    foreach ($trace as $item) {
      $class = ArrayExt::fetch($item, 'class', '');
      $type = ArrayExt::fetch($item, 'type', '');
      $function = ArrayExt::fetch($item, 'function', '');
      $file = ArrayExt::fetch($item, 'file', '');
      $line = ArrayExt::fetch($item, 'line', '');
      $result .= "  ${class}{$type}{$function}() {$file}:{$line}\n";
    }
    return $result;
  }
}

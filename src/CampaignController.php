<?php

namespace Tor;

class CampaignController extends BaseController {
  public static $varNames = array(
    'totalAmount',
    'totalContributors',
    'totalDonations',
  );

  public function getTotalsData() {
    $totals = array();
    $redis = \Resque::redis();
    $keyPrefix = $this->environment_info->name() . "_donationCounter";
    foreach (static::$varNames as $varName) {
      $keyName = "$keyPrefix.$varName";
      $value = $redis->get($keyName);
      $totals[$varName] = floatval($value);
    }
    define('DONATION_MATCH_MAX', 75_000);
    if ($totals['totalAmount'] > DONATION_MATCH_MAX) {
      $totals['amountWithMozilla'] = $totals['totalAmount'] + DONATION_MATCH_MAX;
    } else {
      $totals['amountWithMozilla'] = $totals['totalAmount'] * 2;
    }
    return $totals;
  }

  public function totals($request, $response, $args) {
    $result = array(
      'errors' => array(),
      'data' => array(),
    );
    try {
      $result['data'] = $this->getTotalsData();
    } catch (\Resque_RedisException $e) {
      $this->logger->info("Error connecting to redis", array('exception' => $e, 'trace' => $e->getTrace()));
      $result['errors'][] = 'Error communicating with redis to get campaign totals.';
    }
    return $response->withJson($result);
  }

  public static function is_matching_donation($now) {
    $donateMatchingEndDate = \DateTime::createFromFormat('Y-m-j-H:i:s', '2023-12-31-22:00:00', new \DateTimeZone('UTC'));

    if ($now > $donateMatchingEndDate) {
      return FALSE;
    }
    return TRUE;
  }
}

<?php

namespace Tor;

class IpRateLimiter {
  public $redis;
  public $environment_info;

  public function __construct($container) {
    $this->environment_info = $container->get('environment_info');
    $this->logger = $container->get('logger');
    $this->redis = \Resque::redis();
    $this->settings = $container->get('settings')['ipRateLimiter'];
    $this->maxRequestsPerTimeSpan = $this->settings['maxRequestsPerTimeSpan'];
    $this->timeSpan = $this->settings['timeSpan'];
  }

  function check($request) {
    $keyName = $this->keyName($request);
    list($allowance, $lastCheck) = $this->getIpData($keyName);
    $now = time();
    $timePassed = $now - $lastCheck;
    $allowanceAdjustment = $timePassed * $this->maxRequestsPerTimeSpan / $this->timeSpan;
    $allowance += $allowanceAdjustment;
    if ($allowance < 1) {
      $this->setIpData($keyName, $allowance, $now);
      $ipAddress = $request->getAttribute('ip_address');
      $parsedBody = $request->getParsedBody();
      if (array_key_exists('fields', $parsedBody)) {
          $email = ArrayExt::fetch($parsedBody['fields'], 'email', '');
      } else {
          $email = ArrayExt::fetch($parsedBody, 'email', '');
      }
      throw new IpRateExceeded("Blocked request: rate limit exceeded from IP address $ipAddress (email $email): {$this->maxRequestsPerTimeSpan} requests in {$this->timeSpan} seconds.");
    } elseif ($allowance > $this->maxRequestsPerTimeSpan) {
      $allowance = $this->maxRequestsPerTimeSpan;
    }
    $allowance -= 1;
    $this->setIpData($keyName, $allowance, $now);
  }

  function getIpData($keyName) {
    $data = $this->redis->get($keyName);
    if (is_null($data)) {
      return [$this->maxRequestsPerTimeSpan, time()];
    }
    $struct = unserialize($data, ['allowed_classes', FALSE]);
    if ($struct === FALSE) {
      return [$this->maxRequestsPerTimeSpan, time()];
    }
    return unserialize($data);
  }

  function keyName($request) {
    $ipAddress = $request->getAttribute('ip_address');
    return $this->environment_info->name() . "_rate_limiter_$ipAddress";
  }

  function setIpData($keyName, $allowance, $lastCheck) {
    $data = serialize([$allowance, $lastCheck]);
    $this->redis->set($keyName, $data);
    $this->redis->expire($keyName, $this->timeSpan);
  }
}

<?php

namespace Tor;

use PayPal\Core\PPConfigManager;
use PayPal\CoreComponentTypes\BasicAmountType;
use PayPal\EBLBaseComponents\ActivationDetailsType;
use PayPal\EBLBaseComponents\BillingAgreementDetailsType;
use PayPal\EBLBaseComponents\BillingPeriodDetailsType;
use PayPal\EBLBaseComponents\CreateRecurringPaymentsProfileRequestDetailsType;
use PayPal\EBLBaseComponents\DoExpressCheckoutPaymentRequestDetailsType;
use PayPal\EBLBaseComponents\PaymentDetailsItemType;
use PayPal\EBLBaseComponents\PaymentDetailsType;
use PayPal\EBLBaseComponents\RecurringPaymentsProfileDetailsType;
use PayPal\EBLBaseComponents\ScheduleDetailsType;
use PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType;
use PayPal\PayPalAPI\CreateRecurringPaymentsProfileReq;
use PayPal\PayPalAPI\CreateRecurringPaymentsProfileRequestType;
use PayPal\PayPalAPI\DoExpressCheckoutPaymentReq;
use PayPal\PayPalAPI\DoExpressCheckoutPaymentRequestType;
use PayPal\PayPalAPI\GetExpressCheckoutDetailsReq;
use PayPal\PayPalAPI\GetExpressCheckoutDetailsRequestType;
use PayPal\PayPalAPI\SetExpressCheckoutReq;
use PayPal\PayPalAPI\SetExpressCheckoutRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;
use Slim\Http\Uri;

class PaypalController extends BaseController {
  const CURRENCY_CODE = 'USD';

  public function buildPaymentDetails($amountInDollars) {
    $paymentDetails = new PaymentDetailsType();
    $itemDetails = new PaymentDetailsItemType();
    $itemDetails->Name = "Donation to The Tor Project";
    $itemDetails->Amount = new BasicAmountType(static::CURRENCY_CODE, $amountInDollars);
    $itemDetails->Quantity = 1;
    $paymentDetails->PaymentDetailsItem[] = $itemDetails;
    $paymentDetails->ItemTotal = new BasicAmountType(static::CURRENCY_CODE, $amountInDollars);
    $paymentDetails->OrderTotal = new BasicAmountType(static::CURRENCY_CODE, $amountInDollars);
    $paymentDetails->PaymentAction = 'Sale';
    return $paymentDetails;
  }

  public function processSuccessful($paypalResponse) {
    if ($paypalResponse->Ack == 'Success') {
      return TRUE;
    } else if ($paypalResponse->Ack == 'SuccessWithWarning') {
      if (count($paypalResponse->Errors) == 1) {
	$error = $paypalResponse->Errors[0];
	if ($error->ErrorCode == 11607) {
	  return TRUE;
	}
      }
    }
    return FALSE;
  }

  public function setExpressCheckout($request, $response, $args) {
    $parsedBody = $request->getParsedBody();
    $amount = intval(ArrayExt::fetch($parsedBody, 'amount'));
    $recurring = ArrayExt::fetch($parsedBody, 'recurring', FALSE);
    $amountInDollars = $amount / 100;
    $paymentDetails = $this->buildPaymentDetails($amountInDollars);
    $requestDetails = new SetExpressCheckoutRequestDetailsType();
    $requestDetails->SolutionType = 'Sole';
    $requestDetails->PaymentDetails[] = $paymentDetails;
    if ($recurring) {
      $billingAgreementDetails = new BillingAgreementDetailsType();
      $billingAgreementDetails->BillingType = 'RecurringPayments';
      $billingAgreementDetails->BillingAgreementDescription = 'Recurring Donation to the Tor Project';
      $requestDetails->BillingAgreementDetails[] = $billingAgreementDetails;
    }
    $baseUrl = $request->getUri()->getBaseUrl();
    $requestDetails->CancelURL = $baseUrl . $this->router->pathFor('home');
    $requestDetails->ReturnURL = $baseUrl . $this->router->pathFor('process-paypal');
    $requestType = new SetExpressCheckoutRequestType();
    $requestType->SetExpressCheckoutRequestDetails = $requestDetails;
    $paypalRequest = new SetExpressCheckoutReq();
    $paypalRequest->SetExpressCheckoutRequest = $requestType;
    $paypal = new PayPalAPIInterfaceServiceService();
    $paypalResponse = $paypal->SetExpressCheckout($paypalRequest);
    $responseData = array(
      'errors' => array(),
    );
    if ($paypalResponse->Ack == 'Success') {
      $responseData['token'] = $paypalResponse->Token;
    } else {
      $exception = new ProcessingException($paypalResponse);
      $responseData['errors'] = array_merge($responseData['errors'], $exception->longMessages());
    }
    return $response->withJson($responseData);
  }

  public function getCheckoutDetails($token) {
    $detailsRequestType = new GetExpressCheckoutDetailsRequestType($token);
    $detailsRequest = new GetExpressCheckoutDetailsReq();
    $detailsRequest->GetExpressCheckoutDetailsRequest = $detailsRequestType;
    $paypalService = new PayPalAPIInterfaceServiceService();
    $paypalResponse = $paypalService->GetExpressCheckoutDetailS($detailsRequest);
    if ($paypalResponse->Ack != 'Success') {
      throw new ProcessingException($paypalResponse);
    }
    return $paypalResponse->GetExpressCheckoutDetailsResponseDetails;
  }

  public function process($request, $response, $args, $params) {
    $token = ArrayExt::fetch($params, 'token');
    $responseData = array(
      'errors' => array()
    );
    try {
      $originalDetails = $this->getCheckoutDetails($token);
      $amountInDollars = $originalDetails->PaymentDetails[0]->OrderTotal->value;
      $amount = intval(ArrayExt::fetch($params, 'amount'));
      $recurring = ArrayExt::fetch($params, 'recurring', FALSE);
      if ($recurring) {
	$paypalResponse = $this->processRecurring($request, $amountInDollars, $token);
      } else {
	$paypalResponse = $this->processOneTime($request, $amountInDollars, $token, $params);
      }
      $this->sendDonationInfoToCrm($params, $paypalResponse, $recurring, $amountInDollars);
    } catch (ProcessingException $e) {
      $responseData['errors'] = array_merge($responseData['errors'], $e->longMessages());
    }
    return $responseData;
  }

  /*
   * This is to handle the PayPal first party isolation problem.
   * See the README for details.
   */
  public function processFromGet($request, $response, $args) {
    $params = $request->getQueryParams();
    return $this->renderer->render($response, 'paypal-process.twig', $params);
  }

  public function processFromPost($request, $response, $args) {
    $params = $request->getParsedBody();

    $fields = ArrayExt::fetch($params, "fields");
    $captcha = new \Tor\Captcha();
    $valid = $captcha->is_valid($fields, $_SESSION);
    if (!$valid) {
      $responseData['errors'] = array("Captcha is incorrect, please check your input.");
    }
    else {
      $responseData = $this->process($request, $response, $args, $params);
    }
    return $response->withJson($responseData);
  }

  public function processOneTime($request, $amountInDollars, $token, $params) {
    $payerID = ArrayExt::fetch($params, 'PayerID');
    $paymentDetails = $this->buildPaymentDetails($amountInDollars);
    $requestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
    $requestDetails->PayerID = $payerID;
    $requestDetails->Token = $token;
    $requestDetails->PaymentAction = 'Sale';
    $requestDetails->PaymentDetails[] = $paymentDetails;
    $requestType = new DoExpressCheckoutPaymentRequestType();
    $requestType->DoExpressCheckoutPaymentRequestDetails = $requestDetails;
    $paypalRequest = new DoExpressCheckoutPaymentReq();
    $paypalRequest->DoExpressCheckoutPaymentRequest = $requestType;
    $paypal = new PayPalAPIInterfaceServiceService();
    $paypalResponse = $paypal->DoExpressCheckoutPayment($paypalRequest);
    if (!$this->processSuccessful($paypalResponse)) {
      throw new ProcessingException($paypalResponse);
    }
    return $paypalResponse;
  }

  public function processRecurring($request, $amountInDollars, $token) {
    $activationDetails = new ActivationDetailsType();
    $paymentPeriod = new BillingPeriodDetailsType();
    $paymentPeriod->BillingFrequency = 1;
    $paymentPeriod->BillingPeriod = 'Month';
    $paymentPeriod->Amount = new BasicAmountType(static::CURRENCY_CODE, $amountInDollars);
    $scheduleDetails = new ScheduleDetailsType();
    $scheduleDetails->Description = 'Recurring Donation to the Tor Project';
    $scheduleDetails->ActivationDetails = $activationDetails;
    $scheduleDetails->PaymentPeriod = $paymentPeriod;
    $profileDetails = new RecurringPaymentsProfileDetailsType();
    $now = new \DateTime('now', new \DateTimeZone('UTC'));
    $profileDetails->BillingStartDate = $now->format('Y-m-d\TH:i:s\Z');
    $profileRequestDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
    $profileRequestDetails->Token = $token;
    $profileRequestDetails->ScheduleDetails = $scheduleDetails;
    $profileRequestDetails->RecurringPaymentsProfileDetails = $profileDetails;
    $profileRequestType = new CreateRecurringPaymentsProfileRequestType();
    $profileRequestType->CreateRecurringPaymentsProfileRequestDetails = $profileRequestDetails;
    $profileRequest = new CreateRecurringPaymentsProfileReq();
    $profileRequest->CreateRecurringPaymentsProfileRequest = $profileRequestType;
    $paypal = new PayPalAPIInterfaceServiceService();
    $paypalResponse = $paypal->CreateRecurringPaymentsProfile($profileRequest);
    if (!$this->processSuccessful($paypalResponse)) {
      throw new ProcessingException($paypalResponse);
    }
    return $paypalResponse;
  }

  public function sendDonationInfoToCrm($params, $paypalResponse, $recurring, $amountInDollars) {
    $fieldHelper = new FieldHelper();
    $donationInfo = $fieldHelper->createDonationInfo($params);
    $donationInfo['payment_instrument_id'] = 'PayPal';
    $donationInfo['currency'] = static::CURRENCY_CODE;
    if ($recurring) {
      $details = $paypalResponse->CreateRecurringPaymentsProfileResponseDetails;
      $now = new \DateTime('now', new \DateTimeZone('UTC'));
      $donationInfo['receive_date'] = $now->format('Y-m-d\TH:i:s\Z');
      $donationInfo['trxn_id'] = $details->ProfileID;
      $donationInfo['total_amount'] = $amountInDollars;
      $className = 'Tor\Donation\RecurringContributionSetup';
    } else {
      $paymentInfo = $paypalResponse->DoExpressCheckoutPaymentResponseDetails->PaymentInfo[0];
      $donationInfo['receive_date'] = $paymentInfo->PaymentDate;
      $donationInfo['total_amount'] = $amountInDollars;
      $donationInfo['trxn_id'] = $paymentInfo->TransactionID;
      $className = 'Tor\Donation\OneTimeContribution';
    }
    $crmController = new CrmController($this->container);
    $crmController->sendMessage($className, $donationInfo);
  }
}

<?php

namespace Tor;

class StripeWebhookController extends BaseController {
  public function index($request, $response, $args) {
    $parsedBody = $request->getParsedBody();

    // Verify the event by fetching it from Stripe
    $stripeConfig = StripeConfig::setup();
    $event = \Stripe\Event::retrieve($parsedBody['id']);
    $event_type = $event->type;
    switch ($event_type) {
      case 'invoice.payment_succeeded':
        $this->logger->info("stripe recurring transaction success: " . json_encode($parsedBody) . ", from: " . $_SERVER['REMOTE_ADDR']);
        // Don't process if the amount is 0.
        // Stripe sends these $0 invoices for trial periods, which do not need to be processed by Civi.
        if ($event->data->object->total != 0) {
          $this->process_recurring_payment($event, 'Completed');
        }
        break;
      case 'invoice.payment_failed':
        $this->logger->error("stripe recurring transaction failure: " . json_encode($parsedBody) . ", from: " . $_SERVER['REMOTE_ADDR']);
        $this->process_recurring_payment($event, 'Failed');
        break;
    }
  }

  public function process_recurring_payment($event, $contribution_status_id) {
    $args = array(
      'currency' => 'USD',
      'payment_instrument_id' => 'Credit Card',
      'receive_date' => $receive_date = date("Y-m-d H:i:s", $event->created),
      'total_amount' => ($event->data->object->total)/100,
      'trxn_id' => $event->id,
      'recurring_contribution_transaction_id' => $event->data->object->subscription,
      'contribution_status_id' => $contribution_status_id,
    );

    $crmController = new CrmController($this->container);
    $crmController->sendMessage('Tor\Donation\RecurringContributionOngoing', $args);
  }
}

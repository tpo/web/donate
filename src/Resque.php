<?php

namespace Tor;

class Resque {
  public static function queueName($environment_info) {
    return $environment_info->name() . "_web_donations";
  }
}

import React from 'react';

import {PerkTileDropdown} from './perk_tile_dropdown';
import {PerkImage} from './perk_image';

export function PerkTile(props) {
  const {perk, noPerk, onPerkSelection, selectedPrice, selectedPerk, setPerkOption, perkOption, frequency} = props;

  const classes = ['perk'];
  if (selectedPrice < perk.price[frequency]) {
    classes.push('disabled');
  }
  if (selectedPerk == perk.name) {
    classes.push('selected');
  }

  return (
    <React.Fragment>
      <div name={perk.name} className={classes.join(" ")} price-in-cents={perk.price['frequency']} onClick={(e) => onPerkSelection(event, perk)}>
        <div name={perk.name} className="price-tag-group">
          <div name={perk.name} className="price-tag">${perk.price[frequency]/100}</div>
        </div>
        <h4 name={perk.name} className="perk-label">{perk.friendly_name}</h4>
        <div name={perk.name} className="slides">
          <PerkImage name={perk.name} perk={perk} perkOption={perkOption}/>
        </div>
        <div name={perk.name} className="perk-desc">{perk.description}</div>
        <PerkTileDropdown name={perk.name} options={perk.options} setPerkOption={setPerkOption} selectedPerk={selectedPerk} perk={perk}/>
      </div>
    </React.Fragment>
  );
}

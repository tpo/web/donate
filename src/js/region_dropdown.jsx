import React from 'react';

export function RegionDropdown(props) {
  const {regions, selectedCountry, required, onChange} = props;

  let classes=['field'];
  if (required) {
    classes.push('required');
  }

  const regionsForCountry = regions[selectedCountry];
  if (regionsForCountry == undefined) {
    return null;
  } else {
    let optionElements = [
      (<option key="none" value="">State</option>),
    ];
    for (const region of regionsForCountry) {
      optionElements.push(<option key={region} value={region}>{region}</option>);
    }
    return(
      <select id="region" name="region" className={classes.join(' ')} onChange={onChange}>
        {optionElements}
      </select>
    );
  }
}

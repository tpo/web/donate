import React from 'react';

export function ShirtSizeSelector(props) {
  const { shirtFits, perkOption, shirt, fitsAndSizes, updateFitsAndSizes, sizeOptions, perkOptionProperties } = props;

  let shirtOption = perkOption;
  if (shirt == 'shirt1') {
    shirtOption = 'take-back-internet';
  } else if (shirt == 'shirt2') {
    shirtOption = 'strength-in-numbers';
  }
  let shirtLabel = shirtFits[shirtOption]['friendly-name'];

  const selectFitFieldName = shirt + '-fit';
  const selectSizeFieldName = shirt + '-size';

  const selectNewFit = (event) => {
    const toBeUpdated = event.target.getAttribute('name').split('-');
    const perkToBeUpdated = toBeUpdated[0];
    const perkToBeUpdatedProperty = toBeUpdated[1];
    updateFitsAndSizes(perkToBeUpdated, perkToBeUpdatedProperty, event.target.value);
  }

  return (
    <React.Fragment>
      <div id="selected-perk-fields-label">{shirtLabel}</div>
      <div className="fit-options-div">
        <select name={selectFitFieldName} className="field input fit required" onChange={selectNewFit}>
          <option value={null}>Select Fit</option>
          {Object.keys(shirtFits[shirtOption]['fits']).map(fit =>
            <option value={fit} key={fit}>
              {shirtFits[shirtOption]['fits'][fit]['friendly-name']}
            </option>
          )}
        </select>
        <select name={selectSizeFieldName} className="field input size required" onChange={selectNewFit}>
          <option value={null}>Select Size</option>
          {sizeOptions}
        </select>
      </div>
    </React.Fragment>
  );
}

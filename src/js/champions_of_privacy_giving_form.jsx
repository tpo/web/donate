import React, {useState, useRef, useContext, useEffect} from 'react';
import {injectStripe} from 'react-stripe-elements';
import {GivingForm} from './giving_form';

export function _ChampionsOfPrivacyGivingForm(props) {
  const {stripe} = props;

  return(
    <React.Fragment>
      <GivingForm
        frequency='champions-of-privacy'
        stripe={stripe}
        initialSelectedPrice={100000}
        displayPerkSelection={false}
      />
    </React.Fragment>
  );
}

export const ChampionsOfPrivacyGivingForm = injectStripe(_ChampionsOfPrivacyGivingForm);

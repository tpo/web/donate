import React, {useState, useRef, useContext, useEffect} from 'react';
import {injectStripe} from 'react-stripe-elements';

import {DonationPrices} from './donation_prices';
import {isNotBlank} from './validators';
import {isValidEmail} from './validators';
import {findErrorByName} from './named_error';
import {NamedError} from './named_error';
import {PaymentOptionButton} from './payment_option_button';
import {PerkSelectionSection} from './perk_selection_section';
import {GivingInfoForm} from './giving_info_form';
import {GivingErrorContainer} from './giving_error_container';
import {perks, pricesOnButtons, paymentMethods, shirtFits, sweatshirtSizes, requiredFields, textFields} from './settings';
import {AppContext} from './app_context';
import {LoadingDialogReactPages} from './loading_dialog_react_pages';
import {stripeTokenFieldMap} from './settings';

export function _GivingForm(props) {
  const {frequency, stripe, initialSelectedPrice, displayPerkSelection} = props;
  const [noPerk, setNoPerk] = useState(true);
  const [selectedPrice, setSelectedPrice] = useState(initialSelectedPrice);
  const [selectedPerk, setSelectedPerk] = useState(null);
  const [perkOption, setPerkOption] = useState('strength-in-numbers');
  const [paymentMethod, setPaymentMethod] = useState('credit-card');
  const [mailingListOptIn, setMailingListOptIn] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState([]);
  const [fitsAndSizes, setFitsAndSizes] = useState({
    'shirt1Fit': null,
    'shirt2Fit': null,
    'shirt1Size': null,
    'shirt2Size': null,
    'sweatshirtSize': null
  });
  const [formData, setFormData] = useState({
    'firstName': null,
    'lastName': null,
    'email': null,
    'country': 'US',
    'region': null,
  });

  const priceOtherRef = useRef(null);

  const appContext = useContext(AppContext);

  const requiredFieldsForPerkAndPayment = () => {
    return requiredFields(paymentMethod, selectedPerk);
  }

  const requiredFitAndSizeFields = () => {
    const perk = findPerkByName(selectedPerk);
    if (perk != undefined) {
      return perk.requiredFields;
    }
    return [];
  }

  useEffect(() => {
    const error = findErrorByName(errors, 'donationLessThanTwo');
    if (error != undefined) {
      priceOtherRef.current.focus();
    }
  });

  const addError = (error) => {
    setErrors([...errors, error]);
  }

  const findPerkByName = (name) => {
    return perks.find((perk) => perk.name == name);
  }

  const onPriceChange = (price) => {
    setSelectedPrice(price);
    if (!noPerk) {
      if (price < getPerkPrice(selectedPerk) || !selectedPerk) {
        setSelectedPerk(getDefaultPerk(price));
      }
    }
    if (getDefaultPerk(price) == null) {
      setNoPerk(true);
    }
  }

  const getDefaultPerk = (price) => {
    let defaultPerk = null;
    for (const perk of perks) {
      if (price >= perk.price[frequency]) {
        defaultPerk = perk.name;
      }
    }
    return defaultPerk;
  }

  const getPerkPrice = (perkName) => {
    for (const perk of perks) {
      if (perk.name == perkName) {
        return perk.price[frequency];
      }
    }
  }

  const getSelectedPerkOptionProperties = (selectedPerkOption) => {
    for (const perk of perks) {
      if (perk.options != null) {
        for (const option of perk.options) {
          if (option.name == selectedPerkOption) {
            return option;
          }
        }
      }
    }
  }

  const getPerkFriendlyName = (perkName) => {
    for (const perk of perks) {
      if (perk.name == perkName) {
        return perk.friendly_name;
      }
    }
    return '';
  }

  const onNoPerkCheckboxChange = (event) => {
    if (noPerk) {
      setSelectedPerk(getDefaultPerk(selectedPrice));
    } else {
      setSelectedPerk(null);
    }
    setNoPerk(event.target.checked);
  };

  const getPaymentOptionButtons = () => {
    return (paymentMethods.map(method =>
      <PaymentOptionButton
        key={method.name}
        method={method}
        paymentMethod={paymentMethod}
        onPaymentSelection={onPaymentSelection}
      />
    ));
  }

  const prepareFieldsData = () => {
    let fields = {};
    for (const key in formData) {
      const value = formData[key];
      if (value) {
        fields[key] = value;
      }
    }
    if (noPerk) {
      fields['no-perk-checkbox'] = noPerk;
    }
    if (selectedPerk == 't-shirt-pack'){
      fields['Fit'] = fitsAndSizes['shirt2Fit'];
      fields['Size'] = fitsAndSizes['shirt2Size'];
    } else {
      fields['Fit'] = '--none--';
      fields['Size'] = '--none--';
    }
    return fields;
  };

  const preparePerkData = () => {
    let perkData = {'name': 'none'};
    if (selectedPerk) {
      perkData['name'] = selectedPerk;
      if (selectedPerk == 't-shirt') {
        perkData['size-1'] = fitsAndSizes['shirt1Size'];
        perkData['fit-1'] = fitsAndSizes['shirt1Fit'];
        perkData['style-1'] = 'take-back-internet';
      } else if (selectedPerk == 't-shirt-pack'){
        perkData['style-1'] = perkOption;
        perkData['fit-1'] = fitsAndSizes['shirt2Fit'];
        perkData['fit-2'] = fitsAndSizes['shirt1Fit'];
        perkData['size-1'] = fitsAndSizes['shirt2Size'];
        perkData['size-2'] = fitsAndSizes['shirt1Size'];
      } else if (selectedPerk == 'sweatshirt') {
        perkData['size-1'] = fitsAndSizes['sweatshirtSize'];
      }
    }
    return perkData;
  };

  const validateField = (fieldData, fieldName, validator) => {
    if (!(fieldName in fieldData)) {
      return false;
    }
    return validator(fieldData[fieldName]);
  }

  const getFieldPlaceholder = (fieldName) => {
    if (fieldName in textFields) {
      return textFields[fieldName]['placeholder'];
    }
    return null;
  }

  const validateRequiredFieldsAndDonationAmount = () => {
    const newErrors = [];
    const requiredFields = requiredFieldsForPerkAndPayment();
    for (const fieldName of requiredFields) {
      if (!validateField(formData, fieldName, isNotBlank)) {
        let placeholder = getFieldPlaceholder(fieldName);
        if (placeholder == null) {
          placeholder = fieldName;
        }
        const errorMessage = placeholder + ' must be filled out';
        newErrors.push(new NamedError(fieldName, errorMessage));
      }
    }
    if (!validateField(formData, 'email', isValidEmail)) {
      newErrors.push(new NamedError('email', 'Invalid email'));
    }
    if (selectedPrice < 200) {
      newErrors.push(new NamedError('donationLessThanTwo', '$2 minimum donation'));
    }
    const requiredFitAndSizeFieldNames = requiredFitAndSizeFields();
    for (const fieldName of requiredFitAndSizeFieldNames) {
      if (!validateField(fitsAndSizes, fieldName, isNotBlank)) {
        newErrors.push(new NamedError(fieldName, fieldName + ' must be filled out'));
      }
    }
    return newErrors;
  }

  const createStripeTokenData = () => {
    const tokenData = {};
    for (const fieldName in stripeTokenFieldMap) {
      const stripeName = stripeTokenFieldMap[fieldName];
      if (fieldName in formData) {
        tokenData[stripeName] = formData[fieldName];
      }
    }
    return tokenData;
  }

  const onSubmit = async (e) => {
    if (paymentMethod == 'credit-card') {
      e.preventDefault();
      const newErrors = validateRequiredFieldsAndDonationAmount();
      if (newErrors.length == 0) {
        setLoading(true);
        const tokenData = createStripeTokenData();
        const tokenCreated = await stripe.createToken(tokenData);
        if ('token' in tokenCreated) {
          const token = tokenCreated.token.id;
          const fieldsData = prepareFieldsData();
          const perkData= preparePerkData();
          let recurring = false;
          if (frequency == 'monthly') {
            recurring = true;
          }
          const options = {
            headers: {
              'Content-Type': 'application/json; charset=UTF-8',
            },
            method: 'POST',
            body: JSON.stringify({
              'token': token,
              'amount': selectedPrice,
              'fields': fieldsData,
              'paymentMethod': {'name': 'credit_card'},
              'perk': perkData,
              'recurring': recurring,
            }),
          };
          const result = await fetch('/process-stripe', options);
          const data = await result.json();
          if (data['errors'].length > 0) {
            var errorMsgs = new Array();
            data['errors'].forEach(function(error) {
              errorMsgs.push(error);
            });
            const errorMessage = errorMsgs.join('\n');
            setLoading(false);
            newErrors.push(new NamedError('stripeError', errorMessage));
          } else {
            window.location.href = '/thank-you';
          }
        } else if ('error' in tokenCreated) {
          const errorMessage = tokenCreated['error'].message;
          newErrors.push(new NamedError('stripeError', errorMessage));
          setLoading(false);
        }
      }
      setErrors(newErrors);
    }
  }

  const createBillingAgreement = async (data, actions) => {
    const newErrors = validateRequiredFieldsAndDonationAmount();
    if (newErrors.length > 0) {
      setErrors(newErrors);
      throw new Error("validation errors");
    }
    setErrors([]);
    let recurring = false;
    if (frequency == 'monthly') {
      recurring = true;
    }
    const options = {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({'amount': selectedPrice, 'recurring': recurring}),
    };
    const response = await fetch('/setExpressCheckout', options);
    const response_data = await response.json();
    if ('token' in response_data) {
      return response_data['token'];
    } else if ('errors' in response_data) {
      toggleLoading(false);
      const errorMessage = response_data['errors'].join("\n");
      newErrors.push(new NamedError('paypalError', errorMessage));
      setErrors(newErrors);
      throw new Error(errorMessage);
    } else {
      const body = await response.text();
      const errorMessage = "Invalid response from /setExpressCheckout: " + response.status + ": " + body;
      newErrors.push(new NamedError('paypalError', errorMessage));
      setErrors(newErrors);
      throw new Error(errorMessage);
    }
  };

  const onPerkSelection = (event, perk) => {
    const newPerk = event.target.getAttribute('name');
    if (selectedPrice >= perk.price[frequency]) {
      setNoPerk(false);
      setSelectedPerk(newPerk);
    }
  };

  const onMailingListOptInCheckboxChange = (event) => {
    setMailingListOptIn(event.target.checked);
  }

  const countryChanged = (event) => {
    setFormData({...formData, country: event.target.value});
  };

  const regionChanged = (event) => {
    setFormData({...formData, region: event.target.value});
  };

  const onInputFieldChange = (event) => {
    const fieldName = event.target.getAttribute('name');
    const newState = {...formData};
    newState[fieldName] = event.target.value;
    setFormData(newState);
  };

  const updateFitsAndSizes = (shirt, fitOrSize, newValue) => {
    if (shirt == 'sweatshirt') {
      setFitsAndSizes({...fitsAndSizes, sweatshirtSize: newValue});
    } else {
      const typeName = fitOrSize.charAt(0).toUpperCase() + fitOrSize.slice(1);
      const varName = `${shirt}${typeName}`;
      const newState = {...fitsAndSizes};
      newState[varName] = newValue;
      setFitsAndSizes(newState);
    }
  }

  const onPaymentSelection = (event) => {
    setPaymentMethod(event.target.getAttribute('name'));
    setErrors([]);
  }

  const onStripeFieldChange = (event) => {
    if (event.error != undefined) {
      const newError = new NamedError('stripeError', event.error.message);
      setErrors([...errors, newError]);
    }
  }

  return(
    <React.Fragment>
      <form action="/donate" method="POST" id="donationForm" onSubmit={onSubmit}>
        <div className="donation-selection-area">
          <DonationPrices
            onPriceChange={onPriceChange}
            pricesOnButtons={pricesOnButtons[frequency]}
            selectedPrice={selectedPrice}
            priceOtherUseRef={priceOtherRef}
          />
          <PerkSelectionSection
            displayPerkSelection={displayPerkSelection}
            noPerk={noPerk}
            onNoPerkCheckboxChange={onNoPerkCheckboxChange}
            perks={perks}
            onPerkSelection={onPerkSelection}
            selectedPrice={selectedPrice}
            selectedPerk={selectedPerk}
            setPerkOption={setPerkOption}
            perkOption={perkOption}
            frequency={frequency}
           />
          <div className="totals-area"></div>
        </div>
        <div className="payment-method-area">
          <h4 className="payment-method-question">
            how do you want to <span className="green">DONATE</span>?
          </h4>
          <div className="payment-option">
            {getPaymentOptionButtons()}
          </div>
        </div>
        <div className="info-area">
          <GivingInfoForm
            paymentMethod={paymentMethod}
            mailingListOptIn={mailingListOptIn} onMailingListOptInCheckboxChange={onMailingListOptInCheckboxChange}
            selectedPerk={selectedPerk}
            perkOption={perkOption}
            perkOptionProperties={getSelectedPerkOptionProperties(perkOption)}
            shirtFits={shirtFits}
            sweatshirtSizes={sweatshirtSizes}
            frequency={frequency}
            selectedPrice={selectedPrice}
            noPerk={noPerk}
            selectedPerkFriendlyName={getPerkFriendlyName(selectedPerk)}
            requiredFields={requiredFieldsForPerkAndPayment()}
            errors={errors}
            textFields={textFields}
            stripeSubmitHandle={onSubmit}
            formData={formData}
            countryChanged={countryChanged}
            regionChanged={regionChanged}
            onInputFieldChange={onInputFieldChange}
            fitsAndSizes={fitsAndSizes}
            updateFitsAndSizes={updateFitsAndSizes}
            validateRequiredFieldsAndDonationAmount={validateRequiredFieldsAndDonationAmount}
            preparePerkData={preparePerkData}
            prepareFieldsData={prepareFieldsData}
            createBillingAgreement={createBillingAgreement}
            onStripeFieldChange={onStripeFieldChange}
            addError={addError}
            displayPerkSelection={displayPerkSelection}
          />
        </div>
        <GivingErrorContainer errors={errors} />
      </form>
      <LoadingDialogReactPages open={loading} />
    </React.Fragment>
  );
}

export const GivingForm = injectStripe(_GivingForm);

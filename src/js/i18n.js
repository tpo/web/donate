I18n = {};

I18n.t = function(id, vars) {
  if (vars === undefined) {
    vars = {};
  }
  var el = document.getElementById(id);
  if (el) {
    var message = el.innerHTML;
    for (var key in vars) {
      var regExp = new RegExp('__' + key + '__', 'g');
      message = message.replace(regExp, vars[key]);
    }
    return message;
  }
  else {
    throw `PaymentController Translate: id '${id}' not found`;
  }
}

module.exports = I18n;

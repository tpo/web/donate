import React, {useState, useRef, useContext, useEffect} from 'react';
import {injectStripe} from 'react-stripe-elements';
import {GivingForm} from './giving_form';

export function _MonthlyGivingForm(props) {
  const {stripe} = props;

  return(
    <React.Fragment>
      <GivingForm
        frequency='monthly'
        stripe={stripe}
        initialSelectedPrice={2500}
        displayPerkSelection={true}
      />
    </React.Fragment>
  );
}

export const MonthlyGivingForm = injectStripe(_MonthlyGivingForm);

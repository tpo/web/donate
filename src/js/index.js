import '../../sass/style.scss';
import 'babel-polyfill';
import {CryptocurrencyForm} from './cryptocurrency_form';
import {LoadingDialog} from './loading_dialog';
import {MonthlyGivingForm} from './monthly_giving_form';
import {ChampionsOfPrivacyGivingForm} from './champions_of_privacy_giving_form';
import {PaymentController, GiftMatchingController} from './payment_controller'
import React from 'react';
import ReactDOM from 'react-dom';
import {AppContext} from './app_context';
import {StripeProvider, Elements} from 'react-stripe-elements';

const reactCallbacks = {};
const availableComponents = {
  'CryptocurrencyForm': CryptocurrencyForm,
  'LoadingDialog': LoadingDialog,
  'MonthlyGivingForm': MonthlyGivingForm,
  'ChampionsOfPrivacyGivingForm': ChampionsOfPrivacyGivingForm,
};

const appContext = {};

if ('reactComponents' in window) {
  for (const reactComponent of window.reactComponents) {
    const element = document.getElementById(reactComponent.id);
    const props = reactComponent.props;
    if (element !== null) {
      const ComponentToUse = availableComponents[reactComponent.name];
      let markup = null;
      if ('stripePublishableKey' in props) {
        markup = (
          <AppContext.Provider value={appContext}>
            <StripeProvider apiKey={props.stripePublishableKey}>
              <Elements>
                <ComponentToUse callbacks={reactCallbacks} {...reactComponent.props} />
              </Elements>
            </StripeProvider>
          </AppContext.Provider>
        );
      } else {
        markup = (
          <AppContext.Provider value={appContext}>
              <ComponentToUse callbacks={reactCallbacks} {...reactComponent.props} />
          </AppContext.Provider>
        );
      }
      ReactDOM.render(markup, element);
    }
  }
}

window.tor = {
  PaymentController,
  GiftMatchingController,
  reactCallbacks
};

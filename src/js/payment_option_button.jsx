import React from 'react';

export function PaymentOptionButton(props) {
  const {method, onPaymentSelection, paymentMethod} = props;

  let classes = ['button', 'payment-method'];
  if (method.name == paymentMethod) {
    classes.push('selected');
  }
  if (method.name == 'paypal') {
    classes.push('paypal');
  }

  return (
    <React.Fragment>
      <button type="button" className={classes.join(" ")} name={method.name} onClick={onPaymentSelection}>{method.label}</button>
    </React.Fragment>
  );
}

import React from 'react';

export const perks = [
  {
    'name': 'stickers',
    'image': '/images/Stickerpack-1.png',
    'friendly_name': 'Sticker Pack',
    'description': 'A collection of our favorite logo stickers for decorating your stuff and covering your cams.',
    'price': {
      'monthly': 1000,
    },
    'img_width': 300,
    'img_height': 225,
    'options': null,
    'requiredFields': [],
  },
  {
    'name': 't-shirt',
    'image': '/images/t-shirt-take-back-internet.png',
    'friendly_name': 'T-Shirt',
    'description': 'Get our limited edition Take Back the Internet With Tor shirt.',
    'price': {
      'monthly': 2500,
    },
    'img_width': 214,
    'img_height': 179,
    'options': null,
    'requiredFields': ['shirt1Fit', 'shirt1Size'],
  },
  {
    'name': 't-shirt-pack',
    'image': '/images/t-shirt-pack-take-back-internet.png',
    'friendly_name': 'T-Shirt Pack',
    'description': "Get this year's Take Back the Internet With Tor t-shirt and the Tor: Strength in Numbers t-shirt.",
    'price': {
      'monthly': 5000,
    },
    'img_width': 198,
    'img_height': 140,
    'options': [
      {
        'name': 'strength-in-numbers',
        'friendlyName': 'Strength in Numbers',
        'image': '/images/t-shirt-pack-take-back-internet.png'
      },
    ],
    'requiredFields': ['shirt1Fit', 'shirt1Size', 'shirt2Fit', 'shirt2Size'],
  },
  {
    'name': 'sweatshirt',
    'image': '/images/hoodie-take-back-internet.png',
    'friendly_name': 'Sweatshirt',
    'description': 'Your generous support of Tor gets you this high-quality zip hoodie.',
    'price': {
      'monthly': 10000,
    },
    'img_width': 400,
    'img_height': 283,
    'options': null,
    'requiredFields': ['sweatshirtSize'],
  }
];

export const pricesOnButtons = {
  'monthly': [500, 1000, 2500, 5000, 10000],
  'champions-of-privacy': [100000, 500000, 1000000],
};

export const paymentMethods = [
  {
    'name': 'credit-card',
    'label': 'Credit Card'
  },
  {
    'name': 'paypal',
    'label': (<img name="paypal" className="paypal paypal-png" src="/images/PayPal.svg.png" />),
  }
];

export const shirtFits = {
  'strength-in-numbers': {
    'friendly-name': 'Tor: Strength in Numbers',
    'fits': {
      'slim': {
        'friendly-name': 'Slim',
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
      'classic': {
        'friendly-name': 'Classic',
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
    }
  },
  'pdr': {
    'friendly-name': 'Tor: Powering Digital Resistance',
    'fits': {
      'slim': {
        'friendly-name': 'Slim',
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
      'classic': {
        'friendly-name': 'Classic',
        'sizes': ['s', 'm', 'l', 'xxl']
      },
    }
  },
  'heart-of-internet': {
    'friendly-name': "Tor: the Heart of Internet",
    'fits': {
      'slim': {
        'friendly-name': 'Slim',
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
      'classic': {
        'friendly-name': 'Classic',
        'sizes': ['s', 'm', 'l', 'xxl']
      },
    }
  },
  'ooni': {
    'friendly-name': 'Ooni',
    'fits': {
      'european': {
        'friendly-name': 'European',
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      }
    }
  },
  'take-back-internet': {
    'friendly-name': 'Take back the Internet with Tor',
    'fits': {
      'slim': {
        'friendly-name': 'Slim',
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
      'classic': {
        'friendly-name': 'Classic',
        'sizes': ['s', 'm', 'l', 'xl', 'xxl']
      },
    }
  },
};

export const sweatshirtSizes = ['s', 'm', 'l', 'xl', 'xxl'];

export function requiredFields(paymentMethod, perk) {
  if (paymentMethod == 'credit-card') {
    return ['firstName', 'lastName', 'email', 'streetAddress', 'country', 'locality', 'region', 'postalCode', 'captcha'];
  } else if (perk) {
    return ['firstName', 'lastName', 'email', 'streetAddress', 'country', 'locality', 'region', 'postalCode', 'captcha'];
  } else {
    return ['firstName', 'lastName', 'email', 'captcha'];
  }
};

export const textFields = {
  'firstName': {
    'placeholder': 'First Name',
    'maxLength': 256,
    'type': 'text',
  },
  'lastName': {
    'placeholder': 'Last Name',
    'maxLength': 256,
    'type': 'text',
  },
  'streetAddress': {
    'placeholder': 'Street Address',
    'maxLength': 256,
    'type': 'text',
  },
  'extendedAddress': {
    'placeholder': 'Apt.',
    'maxLength': 256,
    'type': 'text',
  },
  'locality': {
    'placeholder': 'City',
    'maxLength': 256,
    'type': 'text',
  },
  'postalCode': {
    'placeholder': 'Zip',
    'maxLength': 256,
    'type': 'text',
  },
  'email': {
    'placeholder': 'Email Address',
    'maxLength': 256,
    'type': 'text',
  },
  'captcha': {
    'placeholder': 'captcha',
    'type': 'text',
    'maxLength': 5
  },
};

export const stripeTokenFieldMap = {
  'country': 'address_country',
  'email': 'name',
  'extendedAddress': 'address_line2',
  'postalCode': 'address_zip',
  'region': 'address_state',
  'streetAddress': 'address_line1',
  'locality': 'address_city',
};

<?php

namespace Tor;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ErrorHandler extends \Slim\Handlers\Error {
  public $container;

  public function __construct($container) {
    $this->container = $container;
    parent::__construct(FALSE);
  }

  public function __invoke(ServerRequestInterface $request, ResponseInterface $response, \Exception $exception) {
    $response = parent::__invoke($request, $response, $exception);
    $torSiteBaseUrl = $this->container->get('settings')['torSiteBaseUrl'];
    $response = $response->withHeader('Access-Control-Allow-Origin', $torSiteBaseUrl);
    $response = $response->withHeader('Access-Control-Allow-Credentials', 'true');
    $response = $response->withHeader('Access-Control-Allow-Headers', 'Content-Type');
    return $response;
  }

  public function handlePhpError($request, $response, $error) {
  }

  protected function writeToErrorLog($throwable) {
    $message = sprintf('Uncaught Exception %s: "%s" at %s line %s', get_class($throwable), $throwable->getMessage(), $throwable->getFile(), $throwable->getLine());
    $logger = $this->container->get('logger');
    $logger->error($message, array('exception' => $throwable, 'trace' => $throwable->getTrace()));
  }

  protected function renderJsonErrorMessage(\Exception $exception)
  {
    $message = $exception->getMessage();
    $response = [
      'errors' => [
        $message,
      ]
    ];
    return json_encode($response, JSON_PRETTY_PRINT);
  }
}
